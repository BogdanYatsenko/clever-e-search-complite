<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('courses_categories')->insert(['name' => 'ФИЗИКО-МАТЕМАТИЧЕСКИЕ НАУКИ ', 'code' =>  10000]);
DB::table('courses_categories')->insert(['name' => 'ЕСТЕСТВЕННЫЕ НАУКИ ', 'code' =>20000]);
DB::table('courses_categories')->insert(['name' => 'ГУМАНИТАРНЫЕ НАУКИ', 'code' =>30000]);
DB::table('courses_categories')->insert(['name' => 'СОЦИАЛЬНЫЕ НАУКИ ', 'code' =>40000]);
DB::table('courses_categories')->insert(['name' => 'ОБРАЗОВАНИЕ И ПЕДАГОГИКА ', 'code' =>50000]);
DB::table('courses_categories')->insert(['name' => 'ЗДРАВООХРАНЕНИЕ ', 'code' =>60000]);
DB::table('courses_categories')->insert(['name' => 'КУЛЬТУРА И ИСКУССТВО ', 'code' =>70000]);
DB::table('courses_categories')->insert(['name' => 'ЭКОНОМИКА И УПРАВЛЕНИЕ ', 'code' =>80000]);
DB::table('courses_categories')->insert(['name' => 'ИНФОРМАЦИОННАЯ БЕЗОПАСНОСТЬ ', 'code' =>90000]);
DB::table('courses_categories')->insert(['name' => 'СФЕРА ОБСЛУЖИВАНИЯ ', 'code' =>100000]);
DB::table('courses_categories')->insert(['name' => 'СЕЛЬСКОЕ И РЫБНОЕ ХОЗЯЙСТВО ', 'code' =>110000]);
DB::table('courses_categories')->insert(['name' => 'ГЕОДЕЗИЯ И ЗЕМЛЕУСТРОЙСТВО ', 'code' =>120000]);
DB::table('courses_categories')->insert(['name' => 'ГЕОЛОГИЯ, РАЗВЕДКА И РАЗРАБОТКА ПОЛЕЗНЫХ ИСКОПАЕМЫХ ', 'code' =>130000]);
DB::table('courses_categories')->insert(['name' => 'ЭНЕРГЕТИКА, ЭНЕРГЕТИЧЕСКОЕ МАШИНОСТРОЕНИЕ И ЭЛЕКТРОТЕХНИКА', 'code' =>140000]);
DB::table('courses_categories')->insert(['name' => 'МЕТАЛЛУРГИЯ, МАШИНОСТРОЕНИЕ И МАТЕРИАЛООБРАБОТКА ', 'code' =>150000]);
DB::table('courses_categories')->insert(['name' => 'АВИАЦИОННАЯ И РАКЕТНО-КОСМИЧЕСКАЯ ТЕХНИКА ', 'code' =>160000]);
DB::table('courses_categories')->insert(['name' => 'ОРУЖИЕ И СИСТЕМЫ ВООРУЖЕНИЯ ', 'code' =>170000]);
DB::table('courses_categories')->insert(['name' => 'МОРСКАЯ ТЕХНИКА', 'code' =>180000]);
DB::table('courses_categories')->insert(['name' => 'ТРАНСПОРТНЫЕ СРЕДСТВА ', 'code' =>190000]);
DB::table('courses_categories')->insert(['name' => 'ПРИБОРОСТРОЕНИЕ И ОПТОТЕХНИКА ', 'code' =>200000]);
DB::table('courses_categories')->insert(['name' => 'ЭЛЕКТРОННАЯ ТЕХНИКА, РАДИОТЕХНИКА И СВЯЗЬ ', 'code' =>210000]);
DB::table('courses_categories')->insert(['name' => 'АВТОМАТИКА И УПРАВЛЕНИЕ ', 'code' =>220000]);
DB::table('courses_categories')->insert(['name' => 'ИНФОРМАТИКА И ВЫЧИСЛИТЕЛЬНАЯ ТЕХНИКА ', 'code' =>230000]);
DB::table('courses_categories')->insert(['name' => 'ХИМИЧЕСКАЯ И БИОТЕХНОЛОГИИ ', 'code' =>240000]);
DB::table('courses_categories')->insert(['name' => 'ВОСПРОИЗВОДСТВО И ПЕРЕРАБОТКА ЛЕСНЫХ РЕСУРСОВ ', 'code' =>250000]);
DB::table('courses_categories')->insert(['name' => 'ТЕХНОЛОГИЯ ПРОДОВОЛЬСТВЕННЫХ ПРОДУКТОВ И ПОТРЕБИТЕЛЬСКИХ ТОВАРОВ ', 'code' =>260000]);
DB::table('courses_categories')->insert(['name' => 'АРХИТЕКТУРА И СТРОИТЕЛЬСТВО ', 'code' =>270000]);
DB::table('courses_categories')->insert(['name' => 'БЕЗОПАСНОСТЬ ЖИЗНЕДЕЯТЕЛЬНОСТИ, ПРИРОДООБУСТРОЙСТВО И ЗАЩИТА ОКРУЖАЮЩЕЙ СРЕДЫ ', 'code' =>280000]);
DB::table('courses_categories')->insert(['name' => 'ВОЕННОЕ ОБРАЗОВАНИЕ ', 'code' =>290000]);
    }
}
