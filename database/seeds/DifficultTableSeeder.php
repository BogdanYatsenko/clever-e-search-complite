<?php

use Illuminate\Database\Seeder;

class DifficultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses_difficult')->insert(['name' => 'Низкая']);
        DB::table('courses_difficult')->insert(['name' => 'Средняя']);
        DB::table('courses_difficult')->insert(['name' => 'Высокая']);
    }
}
