var token = $("input[name='_token']").val();
$(function() {

    socket.on('session forget', function() {
        document.getElementById('logout-form').submit();
    });

    socket.on('error', function(error) {
        console.warn('Error:', error);
    });

    socket.on('message', function(res) {

        var thread = $('div.conversation[data-thread_id="'+res.thread_id+'"]');
        var msgHtml = $($.parseHTML(res.message));
        var message = msgHtml.find("small.text:last").text();
        var msgtime = msgHtml.find("small.pull-right:last").text();

        if (thread.index() == -1) {
            $.post('/messages/drawthread', {
                _token: token,
                thread_id: res.thread_id
            }).done(function(res) {
                $('div.conversation-wrap').prepend(res['thread_form']);
                $('div.conversation-wrap .active').removeClass('active');
                $('div.conversation:first').addClass('active');

                $(msgHtml).hide().appendTo('div.msg-wrap').fadeIn();
                $('div.message-wrap .msg-wrap')
                    .append('<div class="user-behavior"></div>');
                // $('div.msg-wrap .user-behavior').before(msgHtml.html());
                $('div.msg-wrap').scrollTop($('.msg-wrap')[0].scrollHeight);
            });
        } else {
            if (thread.index() > 0 ) thread.insertBefore('div.conversation:first');
            $('div.conversation:first small').text(message);
            $('div.conversation:first span.msg-time').text(msgtime);
        }

        if (thread.hasClass('active')) {
            // $(res.message).hide().appendTo('div.msg-wrap').fadeIn();
            $('div.msg-wrap .user-behavior').before(res.message);
            $('div.msg-wrap').scrollTop($('.msg-wrap')[0].scrollHeight);
        }

        $('p.text-tips').remove();
        $('div.msg-wrap div.msg-list:last').addClass('unread');
    });

    socket.on('unread', function(res) {

        var thread = $('div.conversation[data-thread_id="'+res.thread_id+'"]');
        var marker = $('div.conversation[data-thread_id="'+res.thread_id+'"] .number');
        var mcount = res.mcount;
        var tcount = res.tcount;

        if (marker.length == 0) {
            thread.append(
                '<div class="msg-count">'+
                '<span class="number">'+mcount+'</span>'+
                '</div>'
            );
        } else thread.find('.number').text(mcount);

        if (tcount > 0) {
            if ($('#messages .tcount').length) {
                $('#messages .tcount').text(tcount);
            } else {

                $('<span class="tcount">'+tcount+'<span>').hide()
                                                .appendTo('#messages .icon')
                                                .fadeIn();
            }
        }
    });

    // socket.on('markers', function(res) {
    //     var data = res.markers;
    //
    //     for (thread_id in data) {
    //         $('div.conversation[data-thread_id="'+thread_id+'"]')
    //         .append(
    //             '<div class="msg-count">'+
    //             '<span class="number">'+data[thread_id]+'</span>'+
    //             '</div>'
    //         );
    //     }
    //
    // });

    // socket.on('conversation unread', function(res) {
    //     var tcount = res.tcount;
    //     $('#messages .icon').append('<span class="tcount">'+tcount+'<span>');
    // });

    socket.on('read', function(res) {
        if ($('div.conversation[data-thread_id="'+res.thread_id+'"]').hasClass('active'))
            $('div.message-wrap div.unread').removeClass('unread');
    });
});
