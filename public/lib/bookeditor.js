$(document).ready(function () {
        /*del book*/
        $("body").on("click", ".delete-cont button", function (e) {
            e.preventDefault();
            var elem = $(this);
            var book_id = elem.data('remove_id');
            var course_id = elem.data('course_id');
            var course_name = elem.data('name');
            $("#deleteConfirmationCourse #deleteBookOk").data("book_id", book_id);
            $("#deleteConfirmationCourse #deleteBookOk").data("course_id", course_id);
            $("#deleteConfirmationCourse .modal-title").text("Удалить курс?");
            $("#deleteConfirmationCourse .modal-body h5").text("Удалить курс: " + course_name + " и все его содержание?");
            // e.preventDefault();
        });

        $("body").on("click", "#deleteBookOk", function (e) {
            var book_id = $(this).data("book_id");
            var course_id = $(this).data("course_id");
            $('.modal-backdrop').hide();
            $.get("/book/edit/cid_"+course_id+"/id"+book_id+"/delbook", function(response){
                $('#panel').html(response.content);
                $('#modals').html(response.modal);
                history.pushState(null, null, '../../../constructor/id'+course_id);
                document.title = response.title;
                upload_avatar();
            });
            e.preventDefault();
        });
        /*end del book*/


        /* add lesson */
        $("body").on("click", ".add-lesson-container", function (e) {
            e.preventDefault();
            var book_id = $(this).data('book_id');
            var name = 'Новый раздел';
            var priority = parseInt($('.accordion .lesson-group:not(.accordion-sub):last .lesson-container:first').data('priority') + 1);
            var _token = $("input[name='_token']").val();

            if (isNaN(priority))
                priority = 1;

            $.post("/book/edit/id" + book_id + "/addlesson", {
                _token: _token,
                name: name,
                book_id: book_id,
                priority: priority
            }, function (response) {
                var content = '<div class="lesson-group"><div class="lesson-container" data-priority="' + priority + '" data-has="0">' +
                    '<div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon"></div>' +
                    '<a href="#"><div class="col-md-7 col-sm-7  col-xs-7 nopadding"><h3 data-lesson_id="' + response + '" data-target="#' + response + '" data-title="' + name + '" data-priority="' + priority + '">' + name + '</h3></div></a>' +
                    '<a href="#" data-toggle="modal" data-target="#deleteConfirmation" data-remove_id="' + priority + '"><div class="col-md-1 col-sm-1 col-xs-1 del-lesson nopadding " data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Удалить раздел"><span class="glyphicon glyphicon-trash"></span></div></a>' +
                    /*'<a href="#"><div class="col-md-1 col-sm-1 col-xs-1 add-new-test nopadding" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Добавить тест"> <span class="glyphicon 	glyphicon glyphicon-check"></span></div></a>'+*/
                    '<a href="#"><div class="col-md-1 col-sm-1 col-xs-1 add-new-sublesson  nopadding" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Добавить главу"><span class="glyphicon glyphicon-plus"></span></div></a>' +
                    '</div>' +
                    '<div class="accordion-sub">' +
                    '</div></div>';
                $('.accordion').append(content);

            });

        });
        /*end  add lesson */

        /* add sub-lesson */
        $("body").on("click", ".add-new-sublesson", function (e) {
            e.preventDefault();
            var elem = $(this);
            var book_id = $('.add-lesson-container').data('book_id');
            var name = 'Новая глава';
            var priority = 1;
            if (elem.parent().parent().parent().find('.accordion-sub .lesson-container:last').length > 0) {
                priority = parseInt(elem.parent().parent().parent().find('.accordion-sub .lesson-container:last').data('priority') + 1);
                if (isNaN(priority))
                    priority = 1;
            }

            var parent_id = elem.parent().parent().find('h3').data("lesson_id");

            _token = $("input[name='_token']").val();

            $.post("/book/edit/id" + book_id + "/addsublesson", {
                _token: _token,
                name: name,
                book_id: book_id,
                priority: priority,
                parent_id: parent_id
            }, function (response) {
                var content = '<div class="lesson-container" data-priority="' + priority + '" data-has="0">' +
                    '<div class="col-md-1 col-sm-1 col-xs-1 nopadding "></div><div class="col-md-1 col-sm-1 col-xs-1 nopadding lesson-icon"></div>' +
                    '<a href="#"><div class="col-md-6 col-sm-6 col-xs-6 nopadding"><h3 data-lesson_id="' + response + '" data-target="#' + response + '" data-title="' + name + '" data-priority="' + priority + '">' + name + '</h3></div></a>' +
                    '<a href="#" data-toggle="modal" data-target="#deleteConfirmationSub" data-remove_id_sub="' + priority + '"><div class="col-md-1 col-sm-1 col-xs-1 del-sublesson nopadding " data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Удалить главу"><span class="glyphicon glyphicon-trash"></span></div></a>' +
                    /* '<a href="#"><div class="col-md-1 col-sm-1 col-xs-1 add-new-subtest  nopadding" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Добавить тест"> <span class="glyphicon 	glyphicon glyphicon-check"></span></div></a>'+*/

                    '</div>';
                elem.parent().parent().parent().find('.accordion-sub').append(content);
                elem.parent().parent().find('.lesson-icon').addClass('lesson-sub-icon');
                elem.parent().parent().find('.lesson-icon').removeClass('lesson-icon');

            });

        });
        /*end  add sub-lesson */

        /*del lesson*/
        $("body").on("click", ".lesson-container .del-lesson", function (e) {
            ;
            var elem = $(this).parent().siblings().find('h3');
            var lesson_id = elem.data('lesson_id');
            var lesson_name = elem.data('title');
            var remove_id = $(this).parent().data('remove_id');
            $("#deleteConfirmation #deleteOk").data("lesson_id", lesson_id);
            $("#deleteConfirmation #deleteOk").data("remove_id", remove_id);
            $("#deleteConfirmation .modal-title").text("Удалить раздел?");
            $("#deleteConfirmation .modal-body h5").text("Удалить раздел: " + lesson_name + " и все его содержание?");
            // e.preventDefault();
        });

        $("body").on("click", "#deleteOk", function (e) {
            var lesson_id = $(this).data("lesson_id");
            var _token = $("input[name='_token']").val();
            var remove_id = $(this).data("remove_id");
            var book_id = $('.add-lesson-container').data('book_id');
            $.post("/book/edit/id" + book_id + "/dellesson", {
                _token: _token,
                lesson_id: lesson_id
            }, function (response) {
                $('.accordion').find("[data-lesson_id='" + lesson_id + "']").parent().parent().parent().parent().remove();
            });
            e.preventDefault();
        });
        /*del lesson*/

        /*del sub-lesson*/
        $("body").on("click", ".lesson-container .del-sublesson", function (e) {
            var elem = $(this).parent().siblings().find('h3');
            var lesson_id = elem.data('lesson_id');
            var lesson_name = elem.data('title');
            var remove_id = $(this).parent().data('remove_id_sub');
            $("#deleteConfirmationSub #deleteSubOk").data("lesson_id", lesson_id);
            $("#deleteConfirmationSub #deleteSubOk").data("remove_id", remove_id);
            $("#deleteConfirmationSub .modal-body h5").text("Удалить главу: " + lesson_name + " и все ее содержимое?");
            // e.preventDefault();
        });

        $("body").on("click", "#deleteSubOk", function (e) {
            var lesson_id = $(this).data("lesson_id");
            var _token = $("input[name='_token']").val();
            var book_id = $('.add-lesson-container').data('book_id');
            $.post("/book/edit/id" + book_id + "/delsublesson", {
                _token: _token,
                lesson_id: lesson_id
            }, function (response) {
                $('.accordion').find("[data-lesson_id='" + lesson_id + "']").parent().parent().parent().remove();
            });
            e.preventDefault();
        });
        /*del sub-lesson*/

        /* lesson click */
        $("body").on("click", ".lesson-container h3", function (e) {
            var elem = $(this);
            var lesson_id = elem.data("lesson_id");
            var book_id = $('.add-lesson-container').data('book_id');
            var _token = $("input[name='_token']").val();
            $.post("/book/edit/id" + book_id + "/getlesson", {
                _token: _token,
                lesson_id: lesson_id
            }, function (response) {
                $('.lesson-content').removeClass('hide_block');
                $('.lesson-content .input-name').val(response.name);
                $('.lesson-content .input-name').data('lesson_id', lesson_id);
                CKEDITOR.instances.mytextarea.setData(response.text);
                $('.save-trigger').data("save-trigger", 0);
                CKEDITOR.instances.mytextarea.getCommand('savecourse').setState(CKEDITOR.TRISTATE_DISABLED);
            });
            e.preventDefault();
        });
        /* end lesson click*/

        /*rename-book*/
        $("body").on("click", ".rename-book", function (e) {
            e.preventDefault();
            $(".book-name").hide();
            $(".book-name-input").removeClass('hidden');

        });

        $("body").on("click", ".rename-book-btn", function (e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var book_id = $('.add-lesson-container').data('book_id');
            var user_id = $(this).data('id');
            var name = $(".book-name-input input").val();
            var _token = $("input[name='_token']").val();
            $.post(link, {
                _token: _token,
                book_id: book_id,
                user_id: user_id,
                name:name
            }, function (response) {
                $(".book-name").text("Книга: "+ $(".book-name-input input").val());
                $(".book-name").show();
                $(".book-name-input").addClass('hidden');
            });


        });
        /*end rename-book*/
    }


);