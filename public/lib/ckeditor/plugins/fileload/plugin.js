CKEDITOR.plugins.add('fileload', {
    init : function(editor) {

        editor.addCommand( 'fileload',  new CKEDITOR.dialogCommand( 'fileload',
            {  } ) );

        editor.ui.addButton('fileload', {
            label : 'Вставить файл',
            command : 'fileload',
            icon : this.path + 'icons/fileload.png',
            toolbar: 'others,100'
        });

        CKEDITOR.dialog.add('fileload', this.path + 'dialogs/fileload.js');
    }
});