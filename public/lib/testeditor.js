$(document).ready(function () {
    $('body').on('click', '.add_question', function () {
        elem = $(this);
        var count = $('.question_block').length + 1;
        block = '<div class = "question_block row col-md-12">' +
                '<div class = "col-md-2 btn btn-default del-question" style = "position:absolute; right: 10px; top: 5px;">Удалить</div>'+
            '<div style = "margin: 10px 0 10px 10px;" class = "title-question">Вопрос № ' + count + '</div>' +
            '<input name = "question_id" class = "hidden" value = "' + count + '" />' +
            '<textarea name = "question_name" class = "question_name hidden"></textarea>' +
            '<div class = "ckeditor divtextarea"></div>' +
            '<div class = "col-md-12 col-xs-12 add_variant btn btn-success">Добавить вариант</div>' +
            '</div>';

        elem.before(block);
    });

    $('body').on('click', '.add_variant', function () {
        elem = $(this);
        add_variant_to_question(elem);
    });

    function add_variant_to_question(elem) {
        var count = elem.parent().find('.variant_block').length + 1;
        block =
            '<div class = "variant_block row">' +
            '<input name = "variant_id" class = "hidden" value = "' + count + '" />' +
            '<div class = "col-md-1">' +
            '<input  name = "is_true" type = "checkbox" class = " col-md-12 checkbox"/>' +
            '</div>' +
            '<div class = "col-md-8">' +
            '<textarea name = "variant_name" class = "col-md-12 variant_name hidden"></textarea>' +
            '<div class = "col-md-12 ckeditor divtextarea"></div>' +
            '</div>' +
                '<div class = "col-md-2 btn btn-default del-variant">Удалить</div>'+
            '</div>';

        elem.before(block);
    }


    $('body').on('click', '.ckeditor', function (e) {
        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].getData();
            var elem = parent.CKEDITOR.instances[name].id;
            $('.' + elem).prev().html(CKEDITOR.instances[name].getData());
            $('.' + elem).prev().prev().html(CKEDITOR.instances[name].getData());
            CKEDITOR.instances[name].destroy(true);
        }
        CKEDITOR.replace(this, {customConfig: '/lib/ckeditor/light.js'});
    });

    $('body').on('click', '.save_quiz', function () {
        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].getData();
            var elem = parent.CKEDITOR.instances[name].id;
            $('.' + elem).prev().html(CKEDITOR.instances[name].getData());
            $('.' + elem).prev().prev().html(CKEDITOR.instances[name].getData());
            CKEDITOR.instances[name].destroy(true);
        }
        var quiz = $(".quiz").serializeArray();
        var link = $(this).data('link');
        var _token = $("input[name='_token']").val();
        $.post(link, {
            _token: _token,
            quiz: quiz
        }, function (response) {
              alert('Тест сохранен');
        });

    });

    $('body').on('click', '.del-question', function () {
        $(this).parent().remove();
        $('.question_block input').each(function( index, element ) {
            var i = index+1;
            $(element).val(''+i+'');
        });
        $('.question_block .title-question').each(function( index, element ) {
            var i = index+1;
            $(element).text('Вопрос №'+i);
        });
    });

    $('body').on('click', '.del-variant', function () {
        var elem = $(this).parent().parent();
        $(this).parent().remove();
        elem = elem.find('.variant_id');

        elem.each(function( index, element ) {
            var i = index+1;
            $(element).val(''+i+'');
        });

    });

});