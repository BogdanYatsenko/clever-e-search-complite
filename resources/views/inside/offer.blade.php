@extends('inside.index')

@section('title', 'Документы ')

@section('modal')
@endsection


@section('content')
    <link rel="stylesheet" href="{{ asset('css/home/body_inside.css') }}">
    <div class="main container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="page-header">
                            <h1>Условия продажи и политика обработки данных</h1>
                        </div>
                        <div class="contacts col-md-10 col-md-offset-1">
                            <ul>
                                <li><a href="/files/home/clever_offer.pdf" target="_blank">Договор публичной оферты</a></li>
                                <li><a href="/files/home/privacy_policy.pdf" target="_blank">Политика обработки персональных данных</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
