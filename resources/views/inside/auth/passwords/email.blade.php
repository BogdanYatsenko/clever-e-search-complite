@extends('inside.index')

@section('title', 'Восстановление пароля')

@section('modal')
@endsection


<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row margin-row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3" id="enter_form">
            <div class="form-title col-md-12 col-sm-12 col-xs-12">
                <h4>Восстановление пароля</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-content">
                <div style="margin-top: 30px;">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="login" placeholder="E-Mail" type="email" class="form-control" name="login" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 ">
                                <button type="submit" class="btn btn-login">
                                    Восстановить
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
