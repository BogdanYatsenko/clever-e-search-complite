<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Clever-e') }}</title>

    @include('inside.layers.header')

</head>
<body >
<div id="callbackurl" data-url=""></div>
<div id="modals">
@yield('modal')
</div>

<div id='loadingDiv'>
    Please wait...  <img src='/img/ajaxload.svg' />
</div>

<script>
    $('#loadingDiv').hide().ajaxStart( function() {
        $(this).show();  // show Loading Div
    } ).ajaxStop ( function(){
        $(this).hide(); // hide loading div
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


<?php
    $pos = strpos($_SERVER['REQUEST_URI'], '/search/');
    echo $pos;
?>

@if($pos === false)
    <div class="navbar navbar-fixed-left" id="menu">
        <ul class="nav navbar-nav">
            @if (Auth::check())
                @include(config('laravel-menu.views.bootstrap-items'), ['items' => $UserBar->roots()])
            @else
                @include(config('laravel-menu.views.bootstrap-items'), ['items' => $GuestBar->roots()])
            @endif
        </ul>
        <div class = "question"></div>
    </div>
@endif



<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}">
                <div class="brand-logo"></div>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="nav navbar-nav navbar">
                <!--
                @if (!Auth::guest())
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <input class="noty_search" data-placement="bottom" data-id="search" data-html="true"
                           data-content="<div id = 'search_result_people'></div><div id = 'search_result_courses'></div>"
                           data-toggle="popover_search" type="text" placeholder="Поиск" id="search">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <a href="#" data-html="true" data-toggle="popover" data-placement="bottom"
                       data-content="<div class = 'noty' style = 'height: 500px; overflow: auto;'></div>">
                                <span class="noty_bell noty_modal glyphicon glyphicon-bell">
                                    <div id="count_noty" class="countmsg"></div>
                                </span>
                    </a>
                </div>
                @endif
                    -->
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li class="{{ (Request::is('login') ? 'active' : '') }}"><a href="{{ url('login') }}"><i
                                    class="fa fa-sign-in"></i> Вход</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->first }}  {{ Auth::user()->second }}
                            @if(Auth::user()->photo_id != null)
                                <img class= "user-logo" src="{{Storage::url('avatars/icon/')}}{{ Auth::user()->id }}.{{ Auth::user()->photo->type }}">
                            @else
                                <img class="user-logo" src="/img/no_avatar_icon.png">
                            @endif
                            <span class="caret"></span>
                            <i class="fa fa-caret-down"> </i></a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::check())
                                @if(Auth::user()->is_admin==1)
                                    <li>
                                        <a href="{{ url('admin') }}"><i class="fa fa-tachometer"></i>
                                            Панель управления</a>
                                    </li>
                                    <li role="presentation" class="divider"></li>
                                @endif
                                <li id ="profile_head">
                                    <a  href="/id{{Auth::id()}}"><i class="fa fa-tachometer"></i>
                                        Профиль</a>
                                </li>
                                <li role="presentation" class="divider"></li>
                                <li id="profile_setting">
                                    <a href="/id{{Auth::id()}}/setting"><i class="fa fa-tachometer"></i>
                                        Настройки</a>
                                </li>
                                <li role="presentation" class="divider"></li>
                                @if(!is_null(Auth::user()->company_admin))
                                        @if(!is_null(Auth::user()->company_admin) and Auth::user()->company_admin->is_moderated == 0)
                                            <li style="pointer-events: none; opacity: 0.6">
                                                <a href="/company_admin/became{{Auth::id()}}"><i class="fa fa-tachometer"></i>
                                                    Заявка на рассмотрении</a>
                                            </li>
                                            <li role="presentation" class="divider"></li>
                                        @elseif((Auth::user()->company_admin->is_moderated == 1))
                                            <li style="pointer-events: none; opacity: 0.6">
                                                <a href="/company_admin/became{{Auth::id()}}"><i class="fa fa-tachometer"></i>
                                                    Проверка прошла успешно</a>
                                            </li>
                                            <li role="presentation" class="divider"></li>

                                        @endif
                                @else
                                        <li>
                                            <a href="/company_admin/became{{Auth::id()}}"><i class="fa fa-tachometer"></i>
                                                Администратор компании</a>
                                        </li>
                                        <li role="presentation" class="divider"></li>
                                @endif

                            @endif
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Выход
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class = "faq col-md-3 col-xs-12" >

        <div class="col-md-12 col-sm-12 col-xs-12" style = "padding: 0">
            <div class="panel-group wrap" id="bs-collapse">

                <div class="panel">
                    <div class="panel-heading close-faq" style="background-color: rgb(0, 177, 166);">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#bs-collapse">

                                <div class = "header">
                                    <div class = "h15"></div>
                                    <span class = "close_btn"> Закрыть</span>
                                </div>
                            </a>
                        </h4>
                    </div>
                </div>

                <div id = "faq_panel">

                </div>

                <div class="panel"><div class="panel-heading"><h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#bs-collapse" href="#faq_element_support">Не нашли нужный ответ?</a>
                            </h4></div><div id="faq_element_support" class="panel-collapse collapse"><div class="panel-body">

                            <div class="form-area">
                                <form role="form">
                                    <h3 style="margin-bottom: 25px; text-align: center;">Задайте вопрос администрации</h3>

                                    @if (Auth::guest())
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="faq_name"  placeholder="Имя" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="faq_email"  placeholder="Email" required>
                                    </div>
                                    @else
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="faq_name" placeholder="{{Auth::user()->first}} {{Auth::user()->second}}" disabled>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <textarea class="form-control" type="textarea" id="faq_message" placeholder="Текст вопроса" maxlength="140" rows="7"></textarea>
                                        <span class="help-block"><p id="characterLeft" class="help-block "></p></span>
                                    </div>

                                    @if (Auth::guest())
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <input id="checkbox_rules" type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}} required> С <a href ="#">порядком обработки персональных данных</a> согласен
                                            </label>
                                        </div>
                                    </div>

                                        <button type="button" id="submit_question_for_quest" name="submit" class="btn btn-primary pull-right" disabled>Задать вопрос</button>

                                        @else
                                        <button type="button" id="submit_question_for_user" name="submit" class="btn btn-primary pull-right">Задать вопрос</button>
                                    @endif




                                </form>
                            </div>


                            </div></div></div>;
                <!-- end of panel -->

                </div>
                <!-- end of panel -->

            </div>
            <!-- end of #bs-collapse  -->

        </div>

</div>

<div class="container-fluid" id="panel">

    @yield('content')

</div>

@include('inside.layers.footer')

</body>
</html>
