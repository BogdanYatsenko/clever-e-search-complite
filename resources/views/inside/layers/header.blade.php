<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<!-- Latest compiled and minified CSS -->
<link href="{{ asset('lib/selectpicker/bootstrap-select.min.css') }}" rel="stylesheet">
<link href="{{ asset('lib/jquery-ui/jquery-ui.css') }}" rel="stylesheet">

<!-- Scripst -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('lib/ckeditor/ckeditor.js') }}"></script>
{{--<script src="{{ asset('lib/typeahead/jquery.typeahead.min.js') }}"></script>--}}
<script type="text/javascript" async
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
</script>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

<!-- add custom lib -->
<script src="{{ asset('lib/swipemenu/slideout.min.js') }}"></script>
<script src="{{ asset('lib/selectpicker/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('lib/swapsies/swapsies.js') }}"></script>
<script src="{{ asset('lib/bookeditor.js') }}"></script>
<script src="{{ asset('lib/testeditor.js') }}"></script>
<script src="{{ asset('lib/taskeditor.js') }}"></script>


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

{{-- require for nodejs --}}
{{--<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js') }}"></script>--}}
{{--<script src="{{ asset('node/settings.js') }}"></script>--}}
{{--<script src="{{ asset('node/messenger/client.js') }}"></script>--}}
