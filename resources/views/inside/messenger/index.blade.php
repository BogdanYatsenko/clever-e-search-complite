@extends('inside.index')

@section('title', 'Чат')

@section('modal')
@endsection


@section('content')

    @include('inside.messenger.partials.flash')

    <link rel="stylesheet" href="/lib/typeahead/jquery.typeahead.min.css">
    <link rel="stylesheet" href="/css/inside/messenger.css">

    <div id="page-inside-wrap" class="container has-messages-api">

        <div class="row">
            <div class="left-topmenu col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="typeahead__container">
                    <div class="typeahead__field">
                        <span class="typeahead__query">
                            <i class="typeahead__search-icon"></i>
                            <input class="js-typeahead-user_v1" name="user_v1[query]" spellcheck="false" type="search" placeholder="Поиск" autocomplete="off">
                        </span>
                    </div>
                </div>
            </div>

            <div class="right-topmenu col-lg-9 col-md-9 col-sm-8 col-xs-6">
                <div class="title-col col-md-4">
                    <span>
                        {{-- <a class="title" href="#"></a> --}}
                    </span>
                </div>
                <div class="btn-verbal col-md-2 col-md-offset-6">
                    <div class="verbal col-md-6"></div>
                    <div class="verbal col-md-6"><i class="glyphicon glyphicon-earphone"></i></div>
                </div>
                <div class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-2 col-xs-10 col-offset-xs-2" style="display: none">
                    {{-- view created dialogs --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="conversation-wrap col-lg-3 col-md-5 col-sm-12 col-xs-12">
                @each('inside.messenger.partials.thread', $threads, 'thread', 'inside.messenger.partials.no-threads')
                {{ $threads->links() }}
            </div>

            <div class="message-wrap col-lg-9 col-md-7 col-sm-12 col-xs-12">
                <div class="msg-wrap infinite-scroll"></div>
                {{-- view messages of persons or group --}}
            </div>

        </div>
    </div>

    {{-- <script type="text/javascript"> var token = '{{ Session::token() }}'; </script> --}}
    <script src="{{ asset('lib/typeahead/jquery.typeahead.min.js') }}"></script>
    <script src="{{ asset('js/inside/messenger-reload.js') }}"></script>
    {{-- <script src="{{ asset('vendor/jscroll.min.js') }}"></script> --}}
@stop
