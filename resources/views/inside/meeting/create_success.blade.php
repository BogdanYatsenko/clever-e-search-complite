@extends('inside.index')

@section('title', 'Создание мероприятия')

@section('content')


    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name centered">
                        Мероприятие успешно создано
                        <br/>
                        За статусом события следите в личном кабинете.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30 centered">
                    <a href="/id{{Auth::id()}}"><button class="btn"><b>Перейти личный кабинет</b></button></a>
                </div>
            </div>
        </div>
    </div>

@endsection