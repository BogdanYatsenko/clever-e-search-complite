@extends('inside.index')

@section('title', 'Мои результаты по курсу')

@section('modal')
@endsection


@section('content')

    <div class="container">
        <div class="row margin-row ">
            <div class="col-md-12 col-sm-12 col-xs-12 user-name">
           Мои результаты по курсу "{{$course->name}}"
            </div>


            <p class="col-md-12 col-sm-12 col-xs-12 ">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-5 col-xs-5">Задание</th>
                        <th class="col-md-1 col-xs-1">Результат</th>
                        <th class="col-md-1 col-xs-1">Дополнительно</th>
                        <th class="col-md-1 col-xs-1">Оценка</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach($materials as $material)

                        @if($material->type == 4)
                        <tr>
                            <td  class="col-md-5 col-xs-5 editor-container">Тест {{$material->name}}</td>


                        @forelse($results as $result)
                            @if ($result->material_id == $material->id)
                                <td  class="col-md-1 col-xs-1"><b>{{$result->result}}</b></td>
                                <td  class="col-md-1 col-xs-1"><button class="btn btn-default my_result_more" action="/course/id{{$course->id}}/quiz{{$material->id}}/result">Подробнее</button> </td>
                                @break
                            @elseif ($result->material_id != $material->id && $loop->last)
                                <td  class="col-md-1 col-xs-1">Не пройдено</td>
                                <td  class="col-md-1 col-xs-1"><button class="btn btn-default start-quiz " action="/course/id{{$course->id}}/quiz{{$material->id}}/start" data-mid="{{$material->id}}">Пройти</button></td>
                            @endif
                            @empty
                                <td  class="col-md-1 col-xs-1">Не пройдено</td>
                                <td  class="col-md-1 col-xs-1"><button class="btn btn-default start-quiz " action="/course/id{{$course->id}}/quiz{{$material->id}}/start" data-mid="{{$material->id}}">Пройти</button></td>
                        @endforelse
                            <td  class="col-md-2 col-xs-2">

                                @foreach($material->mark as $mark)
                                    @if (isset($mark))
                                        @if($material->id == $mark->material_id)
                                            @if (Auth::id() == $mark->student_id)
                                            @if($mark->mark == 1)
                                                Зачтено
                                            @endif
                                            @if($mark->mark == 2)
                                                Не зачтено
                                            @endif
                                            @if($mark->mark == 3)
                                                Удовлетворительно
                                            @endif
                                            @if($mark->mark == 4)
                                                Хорошо
                                            @endif
                                            @if($mark->mark == 5)
                                                Отлично
                                            @endif
                                        @endif
                                            @endif
                                    @else
                                        Выставить оценку
                                    @endif
                                @endforeach

                            </td>
                        </tr>
                        @endif

                        @if($material->type == 5)
                            <tr>
                                <td  class="col-md-3 col-xs-3 editor-container">Задание {{$material->name}} </td>
                                @forelse($results as $result)
                                    @if ($result->material_id == $material->id)
                                        <td  class="col-md-1 col-xs-1"><b>{{$result->result}}</b></td>
                                        <td  class="col-md-1 col-xs-1"><div class="btn btn-default">Подробнее</div> </td>
                                    @elseif ($result->material_id != $material->id && $loop->last)
                                        <td  class="col-md-1 col-xs-1">Не выполнено</td>
                                        <td  class="col-md-1 col-xs-1"><button class="btn btn-default my_result_more " action="/course/id{{$course->id}}/materials">Выполнить</button></td>
                                    @endif
                                @empty
                                    <td  class="col-md-1 col-xs-1">Не выполнено</td>
                                    <td  class="col-md-1 col-xs-1"><button class="btn btn-default my_result_more " action="/course/id{{$course->id}}/materials">Выполнить</button></td>
                                @endforelse
                                @if (false) <td  class="col-md-2 col-xs-2">Зачтено</td> @endif
                            </tr>
                        @endif

                    @endforeach

                    </tbody>
                </table>

            </div>

        </div>
    </div>


@endsection







