@extends('inside.index')
@section('title', 'Курс: '.$course->name)
@section('modal')
@endsection
@section('content')



    <style>
       .panel-group .panel {

            border-radius: 0;

            box-shadow: none;

            border-color: #EEEEEE;

        }


        .panel-default > .panel-heading {

            padding: 0;

            border-radius: 0;

            color: #212121;

            background-color: #FAFAFA;

            border-color: #EEEEEE;

        }


        .panel-title {

            font-size: 14px;


        }


        .panel-title > a {

            display: block;

            padding: 15px;

            text-decoration: none;

            color: #363636 !important;

        }


        .more-less {

            float: right;

            color: #363636;

        }


        .panel-default > .panel-heading + .panel-collapse > .panel-body {

            border-top-color: #EEEEEE;

        }


        .container-fluid{

            padding: 0 !important;


        }


        .videowrapper {

            float: none;

            clear: both;

            width: 100%;

            position: relative;

            padding-bottom: 56.25%;

            padding-top: 25px;

            height: 0;

        }

        .videowrapper iframe {

            position: absolute;

            top: 0;

            left: 0;

            width: 100%;

            height: 100%;

        }
       .cke{
           margin: 3px;
           border: 1px solid #00adea;

       }

       /*
Первичные стили:
С ними работает слайд шоу
*/

       #slides {
           position: relative;
           height: 400px;
           padding: 0px;
           margin: 0px;
           list-style-type: none;
       }

       .slide {
           position: absolute;
           left: 0px;
           top: 0px;
           width: 100%;
           height: 100%;
           opacity: 0;
           z-index: 1;
           table-layout: fixed;
           overflow: auto;


           -webkit-transition: opacity 1s;
           -moz-transition: opacity 1s;
           -o-transition: opacity 1s;
           transition: opacity 1s;
       }

       .showing {
           opacity: 1;
           z-index: 2;
       }

       /*
       Второстепенные стили:
       Внешний вид; можете изменять
       */

       .slide {
           font-size: 110%;
           padding: 40px;
           box-sizing: border-box;
           background: #333;
           color: white;
       }



    </style>


    <script type="text/x-mathjax-config">

      MathJax.Hub.Config({

        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}

      });

    </script>



    <div class="container" style = "padding: 0;">


        @if (Auth::id() == $course->user_id)

            <div class="row" style = "margin-top: 20px;">

                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">

                    <button action="/constructor/id{{$course->id}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b><-- Вернуться в конструктор</b></button>

                </div>


            </div>

        @endif

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">



            <div class="hidden">{{ $gid = 1 }}</div>
            <div class="hidden">{{ $lid = 1 }}</div>
            <div class="hidden">{{ $qid = 1 }}</div>
            <div class="hidden">{{ $wid = 1 }}</div>
            <div class="hidden">{{ $tid = 1 }}</div>
            <div class="hidden">{{ $vid = 1 }}</div>

            <div class="hidden">{{ $i = 0 }}</div>



            @foreach($materials as $material)

                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="heading_{{$loop->index}}" onclick="reply_click(this.id);">

                        <h4 class="panel-title">

                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$loop->index}}" aria-expanded="true" aria-controls="collapse_{{$loop->index}}">

                                <i class="more-less glyphicon glyphicon-plus"></i>
                                {{ $gid }}.
                                @if ($material->type == 1)
                                    Текст:<div class="hidden">{{ $tid++ }}</div>
                                @elseif($material->type == 2)
                                    Видео:<div class="hidden">{{ $vid++ }}</div>
                                @elseif($material->type == 3)
                                    Урок:<div class="hidden">{{ $lid++ }}</div>
                                @elseif($material->type == 4)
                                    Тестирование:<div class="hidden">{{ $qid++ }}</div>
                                @elseif($material->type == 5)
                                    Задание:<div class="hidden">{{ $wid++ }}</div>
                                @endif
                                    <div class="hidden">{{ $gid++ }}</div>{{$material->name}}



                            </a>

                        </h4>

                    </div>

                    <div id="collapse_{{$loop->index}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$loop->index}}">
                        <div class="panel-body">
                            <div style = "max-height: 480px;">
                                <div class = "col-md-2"></div>
                                <div class = "col-md-8" class = "material_block" data-material_id = "{{$material->id}}">

                                    @if ($material->type == 4)
                                        <button class = "btn btn-default start-quiz" action = "/course/id{{$course->id}}/quiz{{$material->id}}/start" data-mid="{{$material->id}}">Пройти тест</button>
                                        @foreach($user_answer as $answer)
                                            @if($material->id == $answer->material_id)
                                                <button class = "btn btn-default result-quiz" action = "/course/id{{$course->id}}/quiz{{$material->id}}/result" data-mid="{{$material->id}}">Результаты теста</button>
                                                @break
                                            @endif
                                        @endforeach

                                    @elseif ($material->type == 5)

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        Задание:
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div style = "padding: 5px;border: 1px #00adea solid; min-height: 100px; width: 100%; resize: none;" >{!! json_decode($material->json)->task_text !!}</div>
                                                    </div>
                                                </div>
                                                <br>
                                                @if (json_decode($material->json)->date_of  != null)
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        Сдать до:
                                                    </div>
                                                    <div class="col-md-3">
                                                        {!! json_decode($material->json)->date_of !!}
                                                    </div>
                                                </div>
                                                <br>
                                                @endif
                                                @if (json_decode($material->json)->comment  != "")
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        Комментарий к заданию:
                                                    </div>
                                                    <div class="col-md-12" >
                                                        <div style = "padding: 5px;border: 1px #00adea solid; min-height: 100px; width: 100%; resize: none;" >{!! json_decode($material->json)->comment !!}</div>
                                                    </div>
                                                </div>
                                                    <br>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                Ответ на задание:
                                            </div>
                                            @if ($user_tasks == '[]')
                                                <div class="col-md-12">
                                                    <div id = "task_div_{{$material->id}}" name = "task_text"  style = "padding: 5px; display:none;border: 1px #00adea solid;  height:300px; width: 100%;" ></div>
                                                    <textarea id = "task_text_{{$material->id}}" name = "task_text"  style = "height:300px; width: 100%; resize: none;" ></textarea>


                                                    <script>
                                                        CKEDITOR.replace('task_text_{{$material->id}}',{customConfig: '/lib/ckeditor/light.js'});
                                                        CKEDITOR.config.resize_enabled = false;
                                                        CKEDITOR.config.width = '100%';
                                                    </script>
                                                </div>
                                                <div class="col-md-12">
                                                    <button class = "btn btn-default btn-right complete-task" action = "/course/id{{$course->id}}/task{{$material->id}}/complete" data-mid="{{$material->id}}">Отправить решение</button>
                                                </div>
                                            @endif
                                            @foreach($user_tasks as $task)
                                                @if ($task->material_id == $material->id)
                                                    <div class="col-md-12">
                                                        <div id = "task_div_{{$material->id}}" name = "task_text"  style = "padding: 5px;border: 1px #00adea solid;  height:300px; width: 100%;" >
                                                            {!! json_decode($task->json)->task_text !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">

                                                    </div>
                                                    @break
                                                @elseif ($loop->last)
                                                    <div class="col-md-12">
                                                        <div id = "task_div_{{$material->id}}" name = "task_text"  style = "padding: 5px; display:none;border: 1px #00adea solid;  height:300px; width: 100%;" ></div>
                                                        <textarea id = "task_text_{{$material->id}}" name = "task_text"  style = "height:300px; width: 100%; resize: none;" ></textarea>


                                                        <script>
                                                            CKEDITOR.replace('task_text_{{$material->id}}',{customConfig: '/lib/ckeditor/light.js'});
                                                            CKEDITOR.config.resize_enabled = false;
                                                            CKEDITOR.config.width = '100%';
                                                        </script>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button class = "btn btn-default btn-right complete-task" action = "/course/id{{$course->id}}/task{{$material->id}}/complete" data-mid="{{$material->id}}">Отправить решение</button>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>

                                    @else


                                        {{--Эта строка выводит просто курсы--}}
                                        {{--{!!  $material->json !!}--}}


                                    {{--Создание слайдера--}}
                                        <div id="carousel">
                                            <ul id="slides" >

                                                @for($j=0; $j < count($slides[$i]); $j++)
                                                    @if($j == 0)
                                                        <li class="slide showing">{!! $slides[$i][$j] !!}</li>
                                                    @else
                                                        <li class="slide">{!! $slides[$i][$j] !!}</li>
                                                    @endif
                                                @endfor
                                            <div class="hidden">{{$i = $i + 1}}</div>

                                            </ul>
                                            <button type="button" class="btn btn-right" id="next_{{$loop->index}}">Далее</button>
                                            <button type="button" class="btn" id="previous_{{$loop->index}}">Назад</button>



                                        <script>
                                            var click_id;
                                            var slides;
                                            var currentSlide=0;

                                            function reply_click(clicked_id)
                                            {
                                                slides = 0;
                                                click_id = clicked_id;
                                                var len_click_id = click_id.length;

                                                var str = "#collapse_"+ click_id.substr(8, len_click_id)+" .panel-body div .col-md-8 #carousel #slides .slide";

                                                slides = document.querySelectorAll(str);
                                                var next = document.getElementById('next_' + click_id.substr(8, len_click_id));
                                                var previous = document.getElementById('previous_' + click_id.substr(8, len_click_id));

                                                next.onclick = function() {

                                                    nextSlide(slides, currentSlide);
                                                };
                                                previous.onclick = function() {
                                                    previousSlide(slides, currentSlide);
                                                };


                                            }

                                            function nextSlide(slides, currentSlide) {
                                                goToSlide(currentSlide+1, slides);
                                            }

                                            function previousSlide(slides, currentSlide) {
                                                goToSlide(currentSlide-1, slides);
                                            }

                                            function goToSlide(n, slides) {
                                                if (n<0) {n = 0;}
                                                if (n >= slides.length) {n = 0;}

                                                slides[currentSlide].className = 'slide';
                                                currentSlide = (n+slides.length)%slides.length;
                                                slides[currentSlide].className = 'slide showing';
                                            }

                                        </script>


                                        </div>

                                    @endif

                                </div>
                                {{--<div class = "col-md-2"></div>--}}
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach


        </div><!-- panel-group -->



    </div>

    <script>



        function toggleIcon(e) {

            $(e.target)

                .prev('.panel-heading')

                .find(".more-less")

                .toggleClass('glyphicon-plus glyphicon-minus');

        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);

        $('.panel-group').on('shown.bs.collapse', toggleIcon);


    </script>



@endsection
