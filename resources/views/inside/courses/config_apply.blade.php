@extends('inside.index')

@section('title', 'Настройки курса')

@section('modal')
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3" id="enter_form">
                <div class="form-title col-md-12 col-sm-12 col-xs-12">
                    <h4>Настройки курса</h4>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 form-content">
                    <div class="row">
                    <div style="margin-top: 30px;">
                        <div class="form-group help-block">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                   Настройки успешно сохранены!
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button action="/constructor/config/id{{$course->id}}" type="button" class="btn-default btn goto_config_course" ><b>Вернуться к настройкам</b></button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button action="/constructor/id{{$course->id}}" type="button" class="btn btn-success goto_edit_course"><b>Перейти в редактор</b></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection