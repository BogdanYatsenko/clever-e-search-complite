@extends('inside.index')

@section('title', 'Карточка курса '.$course->name)

@section('modal')
@endsection


@section('content')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style = "padding: 0;">
                    <header id="header">

                        <div class="slider">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active profile" style = "min-height: 250px; background-color: rgb(0, 177, 166); padding-right: 15px;">
                                        <p class="site-name" >{{$course->name}}</p>
                                        <a class = "site-name " style = "font-size: 12px;"  href=""><b>Источник: {{$course ->source}}</b> </a>
                                    </div>

                                </div>


                                   </div>
                        </div>
                        <nav class="navbar navbar-default" style = "margin-bottom: 0; border-radius: 0;">
                            <div class="navbar-header">

                                <a class="col-xs-12 navbar-brand-course" href="" style = "background-color: #fff;">

                                <div class = "user-avatar-sm">
                                    @if($course->photo_id == '')
                                        <div class = "user-avatar-sm">
                                            <img class="other-course-avatar-pic" src="/img/course/geekbrains.jpg">
                                        </div>
                                    @else
                                        <div class = "user-avatar-sm">
                                            <img class="other-course-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $course->id-1  }}.{{$course->photo->type}}">
                                        </div>
                                    @endif

                                    {{--<img class="course-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $course->id-1  }}.{{$course->photo->type}}">--}}
                                </div>



                                </a>

                                <div style = "font-size: 16pt; position: absolute; right: 10px; top: 5px;">
                                @if ($course->price > 0)
                                        <a href="{{$course->link}}}"><div class = "btn btn-success" style = "background-color: rgb(0, 177, 166);"><b>Цена на источнике: {{$course->price}}</b> р. </div></a>
                                @else
                                        <a href="{{$course->link}}}"><div class = "btn btn-success" style = "background-color: rgb(0, 177, 166);"><b>Бесплатно</b></div></a>
                                @endif
                                </div>
                            </div>
                        </nav>
                    </header>



                    <nav class="navbar navbar-default" style = "margin-bottom: 10px; border-radius: 0; font-size: 1.2em;">
                        <div class="navbar-header" style = "padding: 20px;">
                            <span class="">{{$course->description}}</span><br>

                        </div>
                    </nav>




                </div>
            </div>
        </div>


    </div>
    <br>
@endsection