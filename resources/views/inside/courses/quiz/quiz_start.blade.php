@extends('inside.index')

@section('title', 'Прохождение теста')

@section('modal')

@endsection

@section('content')


    <div class = "container">

        <div class="row">
            <div class="col-md-12 user-name">
                Тест "{{$quiz->name}}"
            </div>
        </div>
        <div class="row">

            <div class="col-md-10">
                <p class="lead progress-text">Завершено 0%</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width: 0;">
                        <span class="sr-only">Завершено %</span>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
               <!-- <p class="lead">Время</p>
                <div class="timer">
                    <span>00:00</span>
                </div>-->
            </div>
            <div class="col-md-12">

            @if (isset($quiz->questions))
            @if (!is_null($quiz->questions) && !empty($quiz->questions))

            @php shuffle($quiz->questions) @endphp

            @foreach($quiz->questions as $question)
            <div class="panel panel-success question-block" @if($loop->iteration > 1) style = "display: none;" @endif">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {!!  $question->name !!}
                    </h3>
                </div>
                <div class="panel-body">



                    @if (isset($question->variant))
                        @php shuffle($question->variant) @endphp
                    @foreach($question->variant as $variant)
                    <div class="row">
                        <div class="answer">
                            <div class="col-md-1">
                                <input name="answer_1" class="answer_checkbox" value="option1"  type="checkbox" data-question_id = "{{$question->id}}" data-variant_id = "{{$variant->id}}">
                            </div>
                            <div class="col-md-11">
                                {!! $variant->name !!}</br></br>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif




                </div>
                <div class="panel-footer">

                    <a class = "btn btn-default result-quiz" action = "/course/id{{$course_id}}/quiz{{$quiz_id}}/result" data-mid="{{$quiz_id}}">Закончить тест</a>
                    <a class="btn btn-success btn-right answer-button" role="button" onclick = "$('.progress-bar-success').css('width', '{{100/count($quiz->questions)*$loop->iteration}}%'); $('.progress-text').html('Завершено {{intval(100/count($quiz->questions)*$loop->iteration)}}%')">Ответить</a>

                </div>
            </div>
            @endforeach

            <div class="panel panel-success question-block" style = "display: none;">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Перейти к результатам
                </h3>
            </div>
            <div class="panel-body">
                <button class = "btn btn-default result-quiz" action = "/course/id{{$course_id}}/quiz{{$quiz_id}}/result" data-mid="{{$quiz_id}}">Результаты теста</button>
            </div>
            <div class="panel-footer">

            </div>
        </div>

            @endif
            @endif



            <div class="col-md-1 column">
            </div>
        </div>
        </div>


    </div>


    <script>
        $(".answer-button").on("click", function(){
           parent_element = $(this).parent().parent();
           parent_element.next(".question-block").show();
           parent_element.hide();

           elems = parent_element.find(".answer_checkbox:checked");

           $.each(elems, function(index, elem){
               question_id = $(elem).data("question_id");
               variant_id = $(elem).data("variant_id");

               $.get( "/quiz/save_user_answer/quiz_id{{$quiz_id}}/question_id"+question_id+"/variant_id"+variant_id, {
               }).done(function(data){

               });

           });

        });
    </script>

@endsection