@extends('inside.index')

@section('title', 'Создание курса')

@section('modal')
    <!-- xhr modla-->
    <div class="modal fade" id="xhrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Кроссерверные материалы</h4>
                </div>
                <div class="modal-body">
                    <h5>Выбирая данный пункт вам будет необходимо предоставлять доступ к материалам на сторонем ресурсе каждому стеденту в <b>ИНДИВИДУАЛЬНОМ</b> порядке.</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-left" id="xhrOK" data-remove_id="" data-lesson_id="" data-dismiss="modal">
                        <span class="glyphicon glyphicon-ok"></span> ОК
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end xhr modla -->
@endsection

@section('content')


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>


    <div class="container has-create-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                        Создание курса
                    </div>
                </div>
            </div>
            <form id ="create-form" role="form" method="POST" action="/course/create">
                {{ csrf_field() }}

            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Название курса:
                            <input id="course_name" class="form-control" placeholder="Введите название курса" name="course_name" required autofocus type="text">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Тип курса:
                            <select id="type" class="selectpicker form-control" name="type_name" title="Выберите тип" required>
                                <option data-id="0"> Он-лайн курс</option>
                                <option data-id="1"> Офф-лайн курс</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Краткое описание:
                            <textarea type="text" rows="3" class="form-control" id="discription" placeholder="Краткое описание" value=""></textarea>
                        </div>
                    </div>
                </div>


                <div class="row" id="hide" style="display: block">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                Полное описание:
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <input type="checkbox" name="hide" id="public" value="yes"> Форматирование текста
                            </div>
                            <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="Полное описание" value=""></textarea>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $("input[name=hide]").click( function () {
                        var el = $("#hide");
                        var el1 =$("#show");
                        if ($(this).attr("checked")) {
                            el.css('visibility', 'visible');
                            el.fadeIn().show();
                            el1.css('visibility', 'hidden');
                            el1.fadeOut(300);
                            } else {
                            el.css('visibility', 'hidden');
                            el.fadeOut(300);
                            el1.css('visibility', 'visible');
                            el1.fadeIn().show();
                            }
                    })

                </script>

                <div class="row" id="show" style="display: none;" >
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                Полное описание:
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <input type="checkbox" name="show" id="public" value="yes"> Форматирование текста
                                <script>
                                    $("input[name=show]").click( function () {
                                        var el = $("#hide");
                                        var el1 =$("#show");
                                        if ($(this).attr("checked")) {
                                            el.css('visibility', 'hidden');
                                            el.fadeOut(300);
                                            el1.css('visibility', 'visible');
                                            el1.fadeIn().show();

                                        } else {
                                            el.css('visibility', 'visible');
                                            el.fadeIn().show();
                                            el1.css('visibility', 'hidden');
                                            el1.fadeOut(300);
                                        }
                                    })
                                </script>
                            </div>

                                <textarea id = "task_text" name = "task_text"  style = "height: 300px; width: 100%; resize: none;" >Введите полное описание здесь </textarea>

                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {

                            CKEDITOR.replace('task_text',{customConfig: '/lib/ckeditor/light.js'});
                            CKEDITOR.config.resize_enabled = false;
                            CKEDITOR.config.height = window.innerHeight - 216 * 2.1;
                            CKEDITOR.config.width = '100%';
                        });

                    </script>
                </div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Минимальная цена при продаже курса:
                        <div class="input-group">
                            <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0">
                            <span class="input-group-addon">
                                <input class="free" aria-label="..." name="free"  type="checkbox">
                                    Бесплатный
                            </span>
                        </div>
                            <div class="help-block hidden help-error">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="xhr_course">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input class="xhr_course" aria-label="..." name="xhr_course"  type="checkbox">
                                     Материалы курса находятся на стороннем ресурсе
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hidden">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Ссылка на материалы:
                            <input id="xhr_link" class="form-control" placeholder="Введите ссылку на материалы курса" name="xhr_link" type="text">
                            <div class="help-block-xhr hidden help-error">

                            </div>
                        </div>
                    </div>
                </div>

                <div id = "offline">

                    <br/>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Длительность курса:
                                <input id="course_length_hours" class="form-control" placeholder="Длительность в часах"   type="number">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Количество занятий:
                                <input id="lessons_count" class="form-control" placeholder="Введите количество занятий"   type="number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Максимальное количество студентов:
                                <input id="max_number_students" class="form-control" placeholder="Максимальное число слушателей"    type="number">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Дата начала курса:
                                <div class="input-group" id="datetimepicker1">
                                    <input type="text" class="form-control" data-format="YYYY-MM-DD HH:MM:SS" />
                                    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Автор:
                                <input id="author" class="form-control" placeholder="Введите имя или название организации"    type="text">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Ссылка на автора:
                                <input id="author_link" class="form-control" placeholder="Введите ссылку на автора"    type="text">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                Укажите сложность:
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <input class="dif_check" aria-label="..." name="difficult_checkbox"  type="checkbox">
                                Не указывать
                            </div>
                            <div id="select_dif">
                                <select id="difficulty" class="selectpicker form-control" name="difficulty_name" placeholder="Сложность" title="Выберите сложность">
                                    <option data-id="1"> Низкая </option>
                                    <option data-id="2"> Средняя </option>
                                    <option data-id="3"> Высокая </option>
                                </select>
                            </div>


                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Место проведения:
                                <input id="address" class="form-control" placeholder="Введите адрес, где будут проходить занятия"    type="text">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Необходимые знания:
                                <textarea type="text" rows="3" class="form-control" id="knowledge_requires" placeholder="Введите компетенции, которыми должен обладать студент" value=""></textarea>

                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Полученные навыки:
                                <textarea type="text" rows="3" class="form-control" id="skills" placeholder="Укажите список навыков, полученных послу прохождения курса" value=""></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Целевая аудитория курса:
                                <textarea type="text" rows="3" class="form-control" id="audience" placeholder="Введите описание целевой аудитории" value=""></textarea>

                            </div>
                        </div>

                    </div>

                </div>

                @if(is_null($company_admin))
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px">
                                    Выберите преподавателя:
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8">
                                    <input class="dif_check" aria-label="..." name="teacher_checkbox"  type="checkbox">
                                    Свой преподаватель
                                </div>

                                <select title="Начните писать имя" name="teacher_selector" class="form-control selectpicker" data-live-search = "true" >
                                    @foreach($teachers as $teacher)
                                    @if(!isset($teacher->user->photo->id))
                                    <option data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b> " data-id="{{$teacher->id_teacher}}" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                    @else
                                    <option data-id="{{$teacher->id_teacher}}" data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->user->id}}.{{$teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px">
                                    Выберите преподавателя:
                                </div>
                                <div class="col-md-4 col-sm-8 col-xs-8">
                                    <input class="dif_check" aria-label="..." name="teacher_checkbox"  type="checkbox">
                                    Свой преподаватель
                                </div>
                                <div class="col-md-9 col-sm-12 col-xs-12" style="padding-left: 0px">
                                    <select title="Найдите преподавателя" name="teacher_selector" class="form-control selectpicker" data-live-search = "true">
                                        @foreach($teachers as $teacher)
                                        @if(!isset($teacher->user->photo->id))
                                        <option data-content="<img src='/img/no_avatar.png' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" data-id="{{$teacher->id_teacher}}" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                        @else
                                        <option data-id="{{$teacher->id_teacher}}" data-content="<img src='{{Storage::url('avatars/')}}{{$teacher->user->id}}.{{$teacher->user->photo->type}}' style='width:35px'/><b>&nbsp;&nbsp;&nbsp;&nbsp;{{$teacher->user->first}} {{$teacher->user->second}}</b>" value="{{$teacher->user->first}} {{$teacher->user->second}}"></option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-right: 0px" id="create_teacher_button_company_admin">

                                </div>

                            </div>
                        </div>
                    </div>
                @endif

                <div class="row" id="buttons" style="margin-top: 20px">
                    <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                        <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                        <button id="add_course" type="submit" class="btn btn-success btn-right"><b>Создать курс</b></button>
                        <div id="all_teachers" ></div>
                    </div>
                </div>


            </div>
            </form>

        <div id="add_teacher_company_admin" >
            <form id ="create-teacher-company_admin-form" role="form" method="POST" action="/company_admin/create_teacher">
                {{ csrf_field() }}
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Фамилия:
                                <input id="second_name_create_teacher" class="form-control" placeholder="Фамилия" required type="text">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Имя:
                                <input id="first_name_create_teacher" class="form-control" placeholder="Имя" required  type="text">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Отчество:
                                <input id="third_name_create_teacher" class="form-control" placeholder="Отчество"   type="text">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Email:
                                <input id="email_create_teacher" class="form-control" placeholder="Emial" required  type="text">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Пароль:
                                <input id="password_create_teacher" class="form-control" placeholder="Пароль"  required type="password">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Подтверждение пароля:
                                <input id="confirm_password_create_teacher" class="form-control" placeholder="Введите пароль повторно"  required type="password">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 50px">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <button id="new_teacher_company_admin" type="submit" class="btn btn-success btn-block"><b>Сохранить</b></button>
                        </div>
                        <div id="after-create-teacher-success-text" class="col-md-4 col-sm-12 col-xs-12">

                        </div>
                        <div id="after-create-teacher-success-button" class="col-md-4 col-sm-12 col-xs-12">

                        </div>
                    </div>

                </div>
                <div class="row hidden" id="all_created_teachers">

                </div>

            </form>
        </div>
    </div>
    </div>

    <script>

        jQuery(function(){
            jQuery('#datetimepicker1').datetimepicker(
                {
                    format: 'YYYY-MM-DD HH:mm'
                }
            );
        });

        $("input[name=difficult_checkbox]").click( function(){
            var el = $("#select_dif");
            if (this.checked) {
                el.slideUp();
            } else {
                el.slideToggle();
            }
        });

        $("input[name=teacher_checkbox]").click( function () {
            var el = $("#create_teacher_button_company_admin");
            if(this.checked){
                el.html("");
                el.html('<button id="create_teacher_company_admin" type="button" class="btn btn-success btn-block"><b>Создать преподавателя?</b></button>').
                click( function () {
                    var create_teacher_company_admin = $("#add_teacher_company_admin");
                    create_teacher_company_admin.slideToggle();
                })
            }
            else{
                el.html("");
                var create_teacher_company_admin = $("#add_teacher_company_admin");
                create_teacher_company_admin.slideUp();
            }
        });
//        $("input[name=description_checkbox]").click( function () {
//            var el = $("#create_teacher_button_company_admin");
//            if(this.checked){
//                el.html("");
//                el.html('<button id="create_teacher_company_admin" type="button" class="btn btn-success btn-block"><b>Создать преподавателя?</b></button>').
//                click( function () {
//                    var create_teacher_company_admin = $("#add_teacher_company_admin");
//                    create_teacher_company_admin.slideToggle();
//                })
//            }
//            else{
//                el.html("");
//                var create_teacher_company_admin = $("#add_teacher_company_admin");
//                create_teacher_company_admin.slideUp();
//            }
//        });

        $('select[name=teacher_selector]').on('change', function () {
            var el = $("#create_teacher_button_company_admin");
            el.html("");
            el.html('<button id="add_selected_teachers_to_course" type="button" class="btn btn-success btn-block"><b>Добавить преподавателя</b></button>').
            unbind().
            on('click', function () {
                var teacher_id = $('select[name=teacher_selector] option:selected').data('id');
                var name = $('select[name=teacher_selector] option:selected').val();
                $("#all_teachers").append('<div id="'+teacher_id+'" style="display: inline-block"><a id="teacher_from_list" data-id = '+teacher_id+'> '+name+' </a><button id="but_'+teacher_id+'" type="button" onclick="delete_a(this.id)">удалить</button>, </div>');

            })


        });


        function delete_a(id) {
            var str = id.slice(4, id.length);
            $("#"+str).remove();
        }

        /* render alert xhr */
        $(document).ready(function () {


            var create_teacher_company_admin = $("#add_teacher_company_admin");
            create_teacher_company_admin.slideUp();


            $("select[name=type_name]").change(function(){
                var el = $("#offline");
                var xhr = $("#xhr_course");
                if($(this).find("option:selected").data('id') == 0)
                {

                    el.css('visibility', 'hidden');
                    el.slideUp();

                    xhr.css('visibility', 'visible');
                    xhr.slideToggle();
                }
                else{
                    xhr.removeAttr('visibility');
                    xhr.css('visibility', 'hidden');
                    xhr.slideUp();

                    el.removeAttr('visibility');
                    el.css('visibility', 'visible');
                    el.slideToggle();
                }
            });


            var el = $("#offline");
            el.css('visibility', 'hidden');
            el.slideUp();


            var xhr = $("#xhr_course");
            xhr.css('visibility', 'hidden');
            xhr.slideToggle();

            $(".xhr_course").click(function (e) {
                if ($('.xhr_course').is(':checked')) {
                    $('#xhrModal').modal('show');
                    $('#xhr_link').parent().parent().parent().removeClass('hidden');
                }
                else
                    $('#xhr_link').parent().parent().parent().addClass('hidden');
            });

        /* end render alert xhr */

            $('.selectpicker').selectpicker({
                size: 7
            });

        });

    </script>

@endsection