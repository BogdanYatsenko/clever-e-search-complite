@extends('inside.index')

@section('title', 'Проверка')

@section('content')
    <div class="container has-create-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div>
                        <video id="video" width="400" height="300" autoplay></video>
                    </div>
                    {{--<video id="gum" playsinline autoplay muted></video>--}}
                    {{--<video id="recorded" playsinline loop></video>--}}

                    {{--<div>--}}
                        {{--<button id="record" disabled>Start Recording</button>--}}
                        {{--<button id="play" disabled>Play</button>--}}
                        {{--<button id="download" disabled>Download</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>


    <script>
        (function () {
            var video = document.getElementById('video'),
                vendorURL = window.URL || window.webkitURL;


            navigator.getMedia = navigator.getUserMedia ||
                                navigator.webkitGetUserMedia ||
                                navigator.mozGetUserMedia ||
                                navigator.msGetUserMedia;

            //Захват видео
            navigator.getMedia({
                video: true,
                audio: false
            }, function (stream) {
                //console.log(stream);
                video.src = vendorURL.createObjectURL(stream);
            }, function (error) {
                console.log = error.code;
            });

        })();


    </script>

    {{--<script src="{{ asset('js/video.js') }}"></script>--}}
@endsection