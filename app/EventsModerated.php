<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsModerated extends Model
{
    protected $table = 'events_moderated';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
