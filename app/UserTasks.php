<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTasks extends Model
{
    protected $table = "user_tasks";
}
