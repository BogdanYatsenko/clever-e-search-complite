<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantNew extends Model
{
    protected $table = 'participants';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
