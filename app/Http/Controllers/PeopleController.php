<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Photo;
use DebugBar;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::paginate(20);
        $photo = Photo::get();

        $view = view('inside.people')->with([
            'users'=>$users,
            'photos'=>$photo
        ]);

        if($request->ajax()) {
            if ($request->page != 0) {

                return [
                    'users' => view('inside.ajax.people')->with([
                        'users'=>$users,
                        'photos'=>$photo
                    ])->render(),
                    'next_page' => $users->nextPageUrl()
                ];
            }
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }

    public function search($query) {

        if($query == 'getmyallpeople'){
            $users = User::with('photo')
                ->paginate(20);
        }else {
            $users = User::with('photo')
                ->where('first', 'like', '%' . $query . '%')
                ->orWhere('second', 'like', '%' . $query . '%')
                ->paginate(20);
        }

        return [
            'users' => view('inside.ajax.people')->with(compact('users'))->render(),
            'next_page' => $users->nextPageUrl()
        ];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function livesearch(Request $req) {
        $query = $req['query'];
        $result = User::with('photo')
        ->where('sex', '=', $query)
        // ->where('first', 'like', '%'.$query.'%')
        // ->orWhere('second', 'like', '%'.$query.'%')
        ->paginate(16);

        return $result;
    }
}
