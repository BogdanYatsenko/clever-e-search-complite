<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use App\Course;
use App\Photo;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Style\Alignment;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Fill;
use Illuminate\Support\Facades\Auth;
use App\OtherCourses;
use App\Search_categories;

class CustomController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function HomePage() {

        $title = 'Главная страница';

        $unique_sources = OtherCourses::select('source')->distinct()->get();
        $max_price_courses = Course::where('is_moderated', 1)->max('price');
        $min_price_courses = Course::where('is_moderated', 1)->min('price');

        $max_price_othercourses = OtherCourses::max('price');
        $min_price_othercourses = OtherCourses::min('price');

        if($max_price_courses >= $max_price_othercourses)
            $max_price = $max_price_courses;
        else
            $max_price = $max_price_othercourses;

        if($min_price_courses >= $min_price_othercourses)
            $min_price = $min_price_othercourses;
        else
            $min_price = $min_price_courses;

        $center = Search_categories::where('id', 1)
            ->select('category_name', 'id', 'id_parent')
            ->get();

        //dd($center);
        $child = Search_categories::where('id_parent', 1)
            ->select('category_name', 'id', 'id_parent')
            ->get();

        return view('home.index', [
            'title'=>$title,
            'min_price'=>$min_price,
            'max_price'=>$max_price,
            'price_from' => 0,
            'price_to' => 'max',
            'sources'=>$unique_sources,
            'selected_source' => 0,
            'center' => $center,
            'child' => $child,
        ]);



    }

    public function LoginPage() {
        return view('inside.index');
    }

    public function UserPage() {
        return view('inside.index');
    }

    public function ContactPage() {
        return view('inside.contacts');
    }

    public function OfferPage() {
        return view('inside.offer');
    }

    public function ChatPage() {
        return view('inside.chat');
    }

    public function identify() {
        return ['id' => Auth::id()];
    }
}
