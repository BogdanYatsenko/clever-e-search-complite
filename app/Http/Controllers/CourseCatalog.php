<?php

namespace App\Http\Controllers;

use App\Search_categories;
use Illuminate\Http\Request;
use App\Course;
use App\OtherCourses;
use Illuminate\Database\Eloquent\Collection;

class CourseCatalog extends Controller
{

    public function index()
    {

        $unique_sources = OtherCourses::select('source')->distinct()->get();

        //dd($unique_sources);

        $max_price_courses = Course::where('is_moderated', 1)->max('price');
        $min_price_courses = Course::where('is_moderated', 1)->min('price');

        $max_price_othercourses = OtherCourses::max('price');
        $min_price_othercourses = OtherCourses::min('price');

        if($max_price_courses >= $max_price_othercourses)
            $max_price = $max_price_courses;
        else
            $max_price = $max_price_othercourses;

        if($min_price_courses >= $min_price_othercourses)
            $min_price = $min_price_othercourses;
        else
            $min_price = $min_price_courses;


        $courses = Course::with(['user', 'photo'])
            ->where('is_moderated', '=', 1)
            ->orderBy('price')
            ->take(15)
            //->get()
            ->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();



        $coursesother = OtherCourses::with('photo')
            ->take(15)
//            ->where('price', '>', 0)
            ->orderBy('price')
            ->select('id', 'name', 'description', 'price', 'link', 'source', 'photo_id')->get();



        $courses_count_othercourses = OtherCourses::count('id');
        $courses_count = Course::where('is_moderated', 1)->count('id');

        $courses_var_count = $courses_count;
        $courses_count = $courses_count + $courses_count_othercourses;

        $courses_var_count = $courses_count_othercourses + $courses_var_count;

        $all = $courses ->merge($coursesother);
        $sorted_all = $all->sortBy('price');
        //dd($sorted_all);
        $categories = Search_categories::all();



        $view = view('inside.courses.catalog_courses', [
            'c_id' => 0,
            'price_from' => 0,
            'price_to' => 'max',
            'courses'=>$sorted_all,
            'categories'=>$categories,
            'min_price' => $min_price,
            'max_price' => $max_price,
            'courses_count' => $courses_count,
            'page' => 1,
            'courses_var_count' => $courses_var_count,
            'order_by' => 0,
            'sources' => $unique_sources,
            'selected_source' => 0,
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
                'materials' => $sections['content']
            ]);
        }

        return $view;
    }

    public function search($source, $searchword, $from, $to, $order_by, $page){

        $unique_sources = OtherCourses::select('source')->distinct()->get();
        $query_our = Course::with(['user', 'photo'])
            ->where('price', '>=', $from)
            ->where('is_moderated', '=', 1);

        $query_other = OtherCourses::with('categories')
        ->where('price', '>=', $from);


        if ($searchword != '0'){
            $query_our = $query_our->where('name', 'like', '%'.$searchword.'%');
            $query_other = $query_other->where('name', 'like', '%'.$searchword.'%');}


        if ($to != 'max'){$query_our = $query_our->where('price', '<=', $to);
            $query_other = $query_other->where('price', '<=', $to);}

        if ($order_by == 0){$query_our = $query_our->orderBy('price');
            $query_other = $query_other->orderBy('price');}

        if ($order_by == 1){$query_our = $query_our->orderBy('price', 'desc');
            $query_other = $query_other->orderBy('price', 'desc');}

        if ($order_by == 2){$query_our = $query_our->orderBy('created_at');
            $query_other = $query_other->orderBy('created_at');}

        if ($order_by == 3){$query_our = $query_our->orderBy('created_at', 'desc');
            $query_other = $query_other->orderBy('created_at', 'desc');}


        if ($source != 0) {
                if ($source == 1){
                    $unique_sources = OtherCourses::select('source')->distinct()->get();
                    $query_buf_our = $query_our;

                    $coures_var_count_our = $query_buf_our->count();

                    $courses_var_count = $coures_var_count_our;

                    //$query = $query->take(15*$page);

                    $query_our = $query_our->take(15*$page);
                    $courses_our = $query_our->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();
                    $courses = $courses_our;
                    if ($order_by == 0) $courses = $courses->sortBy('price');
                    if ($order_by == 1) $courses = $courses->sortByDesc('price');
                    //dd($courses);
                    //$courses = $courses->sortBy('price');


                    $max_price_our = Course::where('is_moderated', '=', 1)->max('price');
                    $min_price_our = Course::where('is_moderated', '=', 1)->min('price');
                    $courses_count_our = Course::where('is_moderated', '=', 1)->count('id');

                    $max_price = $max_price_our;

                    $min_price = $min_price_our;

                    $courses_count = $courses_count_our;

                    $content = view('inside.courses.catalog_courses', [
                        'courses'=>$courses,
                        'searchword' => $searchword,
                        'price_from' => $from,
                        'price_to' => $to,
                        'max_price' => $max_price,
                        'min_price' => $min_price,
                        'order_by' => $order_by,
                        'courses_count' => $courses_count,
                        'page' => $page,
                        'courses_var_count' => $courses_var_count,
                        'sources' => $unique_sources,
                        'selected_source' => 1,
                    ]);

                    if(request()->ajax()) {
//
//            $query = $query->skip(15*$page - 15)->take(15);
//
//            $courses = $query->select('id', 'photo_id', 'name', 'discription', 'user_id', 'price')->get();
//
//            $content = view('inside.courses.catalog_courses', [
//                'courses'=>$courses,
//                'courses_var_count' => $coures_var_count
//            ]);

                        $sections = $content->renderSections();
                        return response()->json([
                            'content' => $sections['content'],
                            'content_courses' => $sections['content_courses'],
                            'modal' => $sections['modal'],
                            'title' => $sections['title'],
                        ]);

                        //$content = $content->renderSections(); // returns an associative array of 'content', 'head' and 'footer'

                    }
                    return $content;
                }
                else{
                    $query_buf_other = $query_other;
                    $coures_var_count_other = $query_buf_other->count();


                    $query_other = $query_other->take(15*$page);

                    $courses_other = $query_other->select('id', 'name', 'description','price', 'source', 'photo_id')->get();
                    //$courses_other = $courses_other->sortBy('price');


                    $max_price_other = OtherCourses::max('price');
                    $min_price_other = OtherCourses::min('price');
                    $categories = Search_categories::get();

                    $content = view('inside.courses.catalog_courses', [
                        'courses'=>$courses_other,
                        'searchword' => $searchword,
                        'price_from' => $from,
                        'price_to' => $to,
                        'max_price' => $max_price_other,
                        'min_price' => $min_price_other,
                        'order_by' => $order_by,
                        'courses_count' => $coures_var_count_other,
                        'page' => $page,
                        'courses_var_count' => $coures_var_count_other,
                        'sources' => $unique_sources,
                        'selected_source' => $source,
                    ]);

                    if(request()->ajax()) {
//
//            $query = $query->skip(15*$page - 15)->take(15);
//
//            $courses = $query->select('id', 'photo_id', 'name', 'discription', 'user_id', 'price')->get();
//
//            $content = view('inside.courses.catalog_courses', [
//                'courses'=>$courses,
//                'courses_var_count' => $coures_var_count
//            ]);

                        $sections = $content->renderSections();
                        return response()->json([
                            'content' => $sections['content'],
                            'content_courses' => $sections['content_courses'],
                            'modal' => $sections['modal'],
                            'title' => $sections['title'],
                        ]);

                        //$content = $content->renderSections(); // returns an associative array of 'content', 'head' and 'footer'

                    }
                    return $content;
                }

        }


        else{
            $unique_sources = OtherCourses::select('source')->distinct()->get();
            $query_buf_our = $query_our;
            $query_buf_other = $query_other;


            $coures_var_count_our = $query_buf_our->count();
            $coures_var_count_other = $query_buf_other->count();



            $courses_var_count = $coures_var_count_our + $coures_var_count_other;

            //$query = $query->take(15*$page);

            $query_our = $query_our->take(15*$page);
            $query_other = $query_other->take(15*$page);



            $courses_our = $query_our->select('id', 'photo_id', 'name', 'description', 'user_id', 'price')->get();
            $courses_other = $query_other->select('id', 'name', 'description','price', 'source', 'photo_id')->get();
            //dd($courses_our, $courses_other);

            /*$keyed_courses_our = $courses_our->KeyBy('name');
            $keyed_courses_other = $courses_other->KeyBy('name');

            dd($keyed_courses_our, $keyed_courses_other);*/
            $courses = $courses_our->toBase()->merge($courses_other);
            if ($order_by == 0) $courses = $courses->sortBy('price');
            if ($order_by == 1) $courses = $courses->sortByDesc('price');
            //dd($courses);
            //$courses = $courses->sortBy('price');

            $categories = Search_categories::get();

            $max_price_our = Course::where('is_moderated', '=', 1)->max('price');
            $min_price_our = Course::where('is_moderated', '=', 1)->min('price');
            $courses_count_our = Course::where('is_moderated', '=', 1)->count('id');

            $max_price_other = OtherCourses::max('price');
            $min_price_other = OtherCourses::min('price');
            $courses_count_other = OtherCourses::count('id');


            if($max_price_our >= $max_price_other)
                $max_price = $max_price_our;
            else
                $max_price = $max_price_other;

            if($min_price_our >= $min_price_other)
                $min_price = $min_price_other;
            else
                $min_price = $min_price_our;

            $courses_count = $courses_count_our + $courses_count_other;


            $content = view('inside.courses.catalog_courses', [
                'courses'=>$courses,
                'searchword' => $searchword,
                'price_from' => $from,
                'price_to' => $to,
                'max_price' => $max_price,
                'min_price' => $min_price,
                'order_by' => $order_by,
                'courses_count' => $courses_count,
                'page' => $page,
                'courses_var_count' => $courses_var_count,
                'sources' => $unique_sources,
                'selected_source' => 0,
            ]);

            if(request()->ajax()) {
//
//            $query = $query->skip(15*$page - 15)->take(15);
//
//            $courses = $query->select('id', 'photo_id', 'name', 'discription', 'user_id', 'price')->get();
//
//            $content = view('inside.courses.catalog_courses', [
//                'courses'=>$courses,
//                'courses_var_count' => $coures_var_count
//            ]);

                $sections = $content->renderSections();
                return response()->json([
                    'content' => $sections['content'],
                    'content_courses' => $sections['content_courses'],
                    'modal' => $sections['modal'],
                    'title' => $sections['title'],
                ]);

                //$content = $content->renderSections(); // returns an associative array of 'content', 'head' and 'footer'

            }
            return $content;
        }



        //dd($courses);


    }
}
