<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.08.2018
 * Time: 10:43
 */

namespace App\Http\Controllers\Auth;


use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialNetAuthController
{
    public function redirect($service){

        return Socialite::driver($service)->redirect();

//        $client_id = '1047157901598-kcgutceam03fg41v8qu6nk7k2ndit7o7.apps.googleusercontent.com'; // Client ID
//        $client_secret = 'flApDERDYe0eSdor7hyISYo9'; // Client secret
//        $redirect_uri = 'http://localhost/PleaseWait'; // Redirect URI
//
//        if (isset($_GET['code'])) {
//            $result = false;
//
//            $params = array(
//                'client_id'     => $client_id,
//                'client_secret' => $client_secret,
//                'redirect_uri'  => $redirect_uri,
//                'grant_type'    => 'authorization_code',
//                'code'          => $_GET['code']
//            );
//
//            $url = 'https://accounts.google.com/o/oauth2/token';
//        }
//
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        $result = curl_exec($curl);
//        curl_close($curl);
//
//        $tokenInfo = json_decode($result, true);
//
//
//
//        if (isset($tokenInfo['access_token'])) {
//            $params['access_token'] = $tokenInfo['access_token'];
//
//            $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
//            if (isset($userInfo['id'])) {
//                $userInfo = $userInfo;
//                $result = true;
//            }
//        }
//
//
//        $conn = mysqli_connect("127.0.0.1:3307", "root", "", "test");
//
//        mysqli_set_charset($conn, "utf8");
//
//
//        //dd($userInfo);
//
//        $firstname = $userInfo['given_name'];
//        $secondname = $userInfo['family_name'];
//        $login = $userInfo['email'];
//        $password = hash('sha256', $firstname.$secondname.$login);
//        $sex = 0;
//        if ($userInfo['gender'] == "male") $sex = 1;
//
//
//        $sql = "SELECT id from users where first = '$firstname' and second = '$secondname' and login = '$login'";
//        $resultsd = mysqli_query($conn, $sql);
//        $row = mysqli_fetch_assoc($resultsd);
//
//        //dd($row);
//
//        if ($row['id']){
//            //dd('lalal');
//            return redirect('/id'.$row['id']);
//        }
//        else{
//            $sql = "insert into users(login, password, first, second, sex) VALUES ('$login', '$password', '$firstname', '$secondname', $sex)";
//            mysqli_query($conn, $sql);
//            $sql = "SELECT id from users where first = '$firstname' and second = '$secondname' and login = '$login'";
//            $resultsd = mysqli_query($conn, $sql);
//            $row = mysqli_fetch_assoc($resultsd);
//
//            return redirect('/id'.$row['id']);
//        }



//        if ($result) {
//            echo "Социальный ID пользователя: " . $userInfo['id'] . '<br />';
//            echo "Имя пользователя: " . $userInfo['name'] . '<br />';
//            echo "Email: " . $userInfo['email'] . '<br />';
//            echo "Ссылка на профиль пользователя: " . $userInfo['link'] . '<br />';
//            echo "Пол пользователя: " . $userInfo['gender'] . '<br />';
//            echo '<img src="' . $userInfo['picture'] . '" />'; echo "<br />";
//        }

        //return $view;

    }

    public function callback($service){
        $user = Socialite::with ( $service )->user ();
        return view ( 'inside.PleaseWait' )->with([
            'man' => $user,
        ])
        ->withService ( $service );
    }

}