<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Photo;
use App\ParticipantNew;
use Carbon\Carbon;
use Cmgmyr\Messenger\MessengerServiceProvider;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use DebugBar;

use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;

use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    /**
    * Show all of the message threads to the user.
    *
    * @return mixed
    */
   public function index(Request $request)
   {
       // All threads, ignore deleted/archived participants
       //$threads = Thread::getAllLatest()->get();

       // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->paginate(20);
       // All threads that user is participating in, with new messages
       // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

       if ($request->page != null) {
           return view('inside.messenger.partials.threads', compact('threads'));
           // inside.messenger.partials.thread', $threads
       }

       $view = view('inside.messenger.index')->with([
           'threads' => $threads,
       ]);

       if(request()->ajax()) {
           $sections = $view->renderSections();
           return response()->json([
               'content' => $sections['content'],
               'modal' => $sections['modal'],
               'title' => $sections['title'],
           ]);
       }

       return $view;
   }

   /**
    * Shows a message thread.
    *
    * @param $id
    * @return mixed
    */
   public function show($id, Request $request)
   {
       try {
           $thread = Thread::findOrFail($id);
       } catch (ModelNotFoundException $e) {
           Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
           return redirect()->route('messages');
       }

       // don't show the current user in list
       $userId = Auth::id();
       // $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
       $thread->markAsRead($userId);

       $participant = Participant::where('thread_id', $thread->id)
       ->where('user_id', $userId)
       ->first();

       $messages = $thread->messages()
       ->where('created_at', '>=', $participant->created_at)
       ->orderBy('id', 'desc')
       ->paginate(30);

       // add info about participants
       // $userConversation = $thread->participants()->get();
       $userConversation = ParticipantNew::where('thread_id', $id)->get();

       $users = array();
       $minLastRead = ParticipantNew::where('user_id', '!=', $userId)->max('last_read');
       foreach ($userConversation as $user) {

           $photoType = !is_null($user->user->photo_id) ?
                $user->user->photo->type : null;

           $users[$user->user_id] = array(
               'first' => $user->user->first,
               'second' => $user->user->second,
               'type' =>  $photoType
           );
       }

       $threadId = $thread->id;
       $view = view('inside.messenger.show', compact('messages', 'users', 'minLastRead', 'threadId'));
       $sections = $view->renderSections();

       if ($request->page != null) {
           return $sections['messages'];
       }

       return response()->json([
           'messages' => $sections['messages'],
           'send_form' => $sections['send_form']
       ]);
   }

   /**
    * Creates a new preview user dialog.
    *
    * @return mixed
    */
   public function create(Request $req)
   {
       $thread = Thread::create([
           'subject' => null,
       ]);
       return $thread->id;
    //    $users = User::where('id', '!=', Auth::id())->get();
    //    return view('inside.messenger.create', compact('users'));
   }

   /**
    * Stores a new message thread.
    *
    * @return mixed
    */
   public function store()
   {
       $input = Input::all();

       $thread = Thread::create([
           'subject' => $input['subject'],
       ]);

       // Message
       Message::create([
           'thread_id' => $thread->id,
           'user_id' => Auth::id(),
           'body' => $input['message'],
       ]);

       // Sender
       Participant::create([
           'thread_id' => $thread->id,
           'user_id' => Auth::id(),
           'last_read' => new Carbon,
       ]);

       // Recipients
       if (Input::has('recipients')) {
           $thread->addParticipant($input['recipients']);
       }

       return redirect()->route('messages');
   }

   /**
    * Adds a new message to a current thread.
    *
    * @param $id
    * @return mixed
    */

   /**
   * send new message
   *
   **/
   public function update(Request $req) {
       $userId = Auth::id();
       $threadId = $req['thread_id'];
       $message = $req['message'];

       if ($threadId == 0) {
           $thread = Thread::create();
           $threadId = $thread->id;

           // Sender
           Participant::create([
               'thread_id' => $threadId,
               'user_id' => $userId,
               'last_read' => new Carbon
           ]);

           // Recipients
           $thread->addParticipant($req['participant_id']);
       }

       // Message
       Message::create([
           'thread_id' => $threadId,
           'user_id' => $userId,
           'body' => $message,
       ]);

       Participant::where('user_id', $userId)
       ->where('thread_id', $threadId)
       ->update(['last_read' => now()]);

       // Participant::where('thread_id', $threadId)
       // ->update(['deleted_at' => null]);

       DB::update('update participants set created_at = now(), deleted_at = null where (thread_id = ? AND deleted_at IS NOT NULL AND id <> 0)', [$threadId]);

       $message = Message::where('thread_id', '=', $threadId)
       ->where('user_id', '=', $userId)
       ->latest()
       ->first();

       $thisUser = User::where('id', $userId)->first();

       $users[$userId] = array(
         'first' => $thisUser->first,
         'second' => $thisUser->second,
         'type' => $thisUser->photo->type
       );

       $minLastRead = null;

       return [
               'message' =>  view('inside.messenger.partials.message', compact('message', 'users', 'minLastRead'))->render(),
               'thread_id' => $threadId
           ];

   }

   public function check(Request $req) {
       $thisUser = Auth::id();
       $anotherUser = $req['user_id'];

       if ($thisUser == $anotherUser) return -1;

       $target = DB::table(DB::raw('(select *from  participants
       where user_id = '.$thisUser.') as tb1'))
       ->join(DB::raw('(select *from  participants
       where user_id = '.$anotherUser.') as tb2'), 'tb2.thread_id', '=', 'tb1.thread_id')
       ->join('threads', 'tb1.thread_id', '=', 'threads.id')
       ->whereNull('threads.subject')
       ->first();

       if (empty($target)) return ['thread_id' => 0];

       $thread = Thread::findOrFail($target->id);

       $thread_id = $target->id;

       ParticipantNew::where('user_id', $thisUser)
       ->where('thread_id', $thread_id)
       ->whereNotNull('deleted_at')
       ->update(['deleted_at' => null, 'created_at' => now()]);

       return [
           'thread_form' => view('inside.messenger.partials.thread', compact('thread'))->render(),
           'thread_id' => $target->id
       ];

   }

   public function drawthread(Request $req) {
       $thread = Thread::findOrFail($req['thread_id']);
       return [
           'thread_form' => view('inside.messenger.partials.thread',
           compact('thread'))->render(),
           'thread_id' => $thread->id,
       ];
   }

   public function unread() {
       $tcount = 0;
       if (Auth::id()) {
           $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();

           foreach ($threads as &$thread) {
               if ($thread->userUnreadMessagesCount(Auth::id()) > 0)
                    $tcount++;
            }
       }

       return $tcount;
   }

   public function remove(Request $req) {
       $thread_id = $req['thread_id'];

       Participant::where('user_id', Auth::id())
       ->where('thread_id', $thread_id)
       ->update(['deleted_at' => now()]);

       // get last date user conversation
       $lastDate = Participant::where('thread_id', $thread_id)->min('created_at');
       Message::where('thread_id', $thread_id)
       ->where('created_at', '<', $lastDate)
       ->update(['deleted_at' => now()]);

       return 1;
   }


    public function test_rtc(Request $request)
    {
        $view = view('inside.messenger.test')->with([
            'threads' => 0,
        ]);

        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'content' => $sections['content'],
                'modal' => $sections['modal'],
                'title' => $sections['title'],
            ]);
        }

        return $view;
    }
}
