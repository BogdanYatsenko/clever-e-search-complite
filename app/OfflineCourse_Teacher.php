<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.10.2018
 * Time: 10:09
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class OfflineCourse_Teacher extends Model
{
    protected $table = 'offlinecourses_teachers';

    public function teacher(){
        return $this->belongsTo('App\Teacher', 'id_teacher', 'id_teacher');
    }

    public function offline(){
        return $this->belongsTo('App\OfflineCourse', 'id_offlinecourse', 'id');
    }
}