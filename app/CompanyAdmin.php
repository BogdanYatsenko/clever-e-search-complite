<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.10.2018
 * Time: 12:29
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CompanyAdmin extends Model
{
    protected $table = 'company_admin';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}