<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.10.2018
 * Time: 11:39
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{

    protected $table = 'meetings';

    public function teacher(){
        return $this->hasMany('App\Meeting_Teacher', 'id_meeting', 'id_meeting');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user_creator', 'id');
    }

}